# Gitlab CI-CD Remote Template

This repository serves as a GitLab CI/CD remote template that can be used by other projects on GitLab. It provides a starting point for setting up CI/CD pipelines in other repositories.

## Overview

The purpose of this repository is to offer a standardized CI/CD configuration that can be easily integrated into other projects hosted on GitLab. By using this template, projects can quickly set up their CI/CD pipelines without needing to create the configuration from scratch.

![CI/CD Flow](gitlab-cicd-flow.png)

## Features

- Standardized CI/CD configuration
- Easy integration into other GitLab projects
- Flexibility to customize pipelines as needed

## Usage

To use this repository as a GitLab CI/CD remote template in another project, follow these steps:

1. Navigate to the Settings of the GitLab repository where you want to use this template.

2. In the CI/CD settings, find the "General pipelines" section.

3. Scroll down to the "CI/CD configuration file" section and add the following file path:
   - `.gitlab-ci.yml@aungmyatkyaw-cicd/gitlab-ci-cd-remote-template:main`

4. Save the changes.

Now, your project is set up to use this repository as a remote template for CI/CD configuration.

### Sample Projects
- [Sample Frontend Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/angular-frontend-example/)
- [Sample Backend Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/spring-boot-3-rest-api-example/)
- [Sample .NET Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/dotnet-6-rest-api-example)

## Configuration

The configuration provided in this repository includes a `.gitlab-ci.yml` file that defines the CI/CD pipelines. You can customize this file to suit the specific needs of your project. Refer to the GitLab CI/CD documentation for detailed information on configuring pipelines: [GitLab CI/CD Documentation](https://docs.gitlab.com/ee/ci/)

In addition to the main CI/CD configuration file, this repository also includes a `.gitlab-ci-base-image-build.yml` file for building base images for jobs. This file can also be added in the CI/CD settings with `.gitlab-ci-base-image-build.yml@aungmyatkyaw-cicd/gitlab-ci-cd-remote-template:main` to define jobs for building base images.

## Contributing

If you encounter any issues or have suggestions for improvements to this template, please feel free to contribute! You can submit bug reports, feature requests, or pull requests through the GitLab repository for this project.

## Support

For support or assistance, you can reach out to the project maintainers via the issue tracker on the GitLab repository.

## Acknowledgements

We would like to acknowledge the contributions of all individuals who have helped improve this template.

## Additional Information

For additional information or resources related to GitLab CI/CD, you can refer to the official documentation: [GitLab CI/CD Documentation](https://docs.gitlab.com/ee/ci/)
